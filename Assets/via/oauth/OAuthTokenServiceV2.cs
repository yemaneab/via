﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using static Assets.Vuforia.Scripts.ViaDomain;
using static Assets.Vuforia.Scripts.ViaDomain.AlertResponse;
using static Assets.Vuforia.Scripts.ViaDomain.BusResponse;

namespace Assets.Vuforia.Scripts
{
    public class OAuthTokenServiceV2
    {
        private string token;
        public OAuthTokenServiceV2()
        {
             //token = GetNewAccessToken();
            }
     

        private  string GetNewAccessToken()
        {
            var postMessage = new Dictionary<string, string>
            {
                {"grant_type", OAuthSettings.GrantType },
                {"client_id", OAuthSettings.ClientId},
                {"client_secret", OAuthSettings.ClientSecret },
                { "Resource", OAuthSettings.GraphResourceld}
            };


            var request = new HttpRequestMessage(HttpMethod.Post, "https://login.microsoftonline.com/2a3033c2-ad76-426c-9c5a-23233cde4cde/oauth2/token")
            {
                Content = new FormUrlEncodedContent(postMessage)
            };

            using (var client = new HttpClient())
            {
                var clientCreds = System.Text.Encoding.UTF8.GetBytes($"{OAuthSettings.ClientId}:{OAuthSettings.ClientSecret}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", System.Convert.ToBase64String(clientCreds));
                client.DefaultRequestHeaders.Add("ContentType", "application/json");

                var tokenResponse = client.SendAsync(request).Result;

                var tokenJson = tokenResponse.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<OAuthToken>(tokenJson).AccessToken;
            }
        }

        public  List<Alert> getAlertsForRoute(string routeId)
        {
            token = GetNewAccessToken();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = client.GetAsync("https://codegtfsapi.viainfo.net/api/v1/bus-alerts/route/" + routeId).Result.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<AlertResponse>(response).result;

            }
        }

        public  List<BusLocation> getBusLocations(string stopId, string routeId, string max)
        {
            token = GetNewAccessToken();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = client.GetAsync("https://codegtfsapi.viainfo.net/api/v1/bus-locations/" + stopId + "/" + routeId + "/" + max).Result.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<BusResponse>(response).result;

            }
        }

    }
}
