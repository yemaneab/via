﻿using System.Collections.Generic;

namespace Assets.Vuforia.Scripts
{
    public class ViaDomain
    {

        public class AlertResponse
        {
            public List<Alert> result { get; set; }
            public int statusCode { get; set; }

            public class Alert
            {
                public long id { get; set; }
                public bool isDeleted { get; set; }
                public long cause { get; set; }
                public long effect { get; set; }
                public string name { get; set; }
                public string description { get; set; }
                public List<Informedentity> informedEntities { get; set; }
                public List<Alertactivedate> alertActiveDates { get; set; }
            }

            public class Informedentity
            {
                public long id { get; set; }
                public long routeId { get; set; }
                public long stopId { get; set; }
                public long busAlertId { get; set; }
            }

            public class Alertactivedate
            {
                public long id { get; set; }
                public long start { get; set; }
                public long end { get; set; }
                public long alertId { get; set; }
            }
        }
        public class BusResponse
        {
            public List<BusLocation> result { get; set; }
            public int statusCode { get; set; }

            public class BusLocation
            {
                public long vehicle { get; set; }
                public long busLabel { get; set; }
                public long scheduledArrival { get; set; }
                public long scheduledDeparture { get; set; }
                public long departureDelay { get; set; }
                public long arrivalDelay { get; set; }
                public long estimatedArrival { get; set; }
                public long estimatedDeparture { get; set; }
                public bool isScheduledTime { get; set; }
                public string eta { get; set; }
                public string etd { get; set; }
                public long tripId { get; set; }
            }

        }

    }
}
