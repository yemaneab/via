﻿using Newtonsoft.Json;
using ProtoBuf;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using transit_realtime;


namespace Assets.Vuforia.Scripts.via.gtfs
{
    class GtfsService
    {
        string tripUpdatesUrl = "http://gtfs.viainfo.net/tripupdate/tripupdates.pb";
        string vehiclePositionsUrl = "http://gtfs.viainfo.net/vehicle/vehiclepositions.pb";
        string alertsUrl = "http://gtfs.viainfo.net/alert/alerts.pb";
        string trapezerealtimefeedUrl = "http://gtfs.viainfo.net/gtfs-realtime/trapezerealtimefeed.pb";
              
        public List<TripUpdate> getTripUpdates(string routeId)
        {
            WebRequest req = WebRequest.Create(tripUpdatesUrl);
            FeedMessage feed = Serializer.Deserialize<FeedMessage>(req.GetResponse().GetResponseStream());

            Debug.WriteLine("\n################################   TRIPS   ##################################################");
            feed.entity.Where(e => e.trip_update?.trip?.route_id.Equals(routeId) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });


            return feed.entity.Where(t => t.trip_update?.trip?.route_id.Equals(routeId) ?? false).Select( t => t.trip_update).ToList(); 
        }

        public List<VehiclePosition> getVehiclePositions(string vehicleId)
        {
            WebRequest req = WebRequest.Create(vehiclePositionsUrl);
            FeedMessage feed = Serializer.Deserialize<FeedMessage>(req.GetResponse().GetResponseStream());

            Debug.WriteLine("\n################################   VEHICLE POSITIONS   ##################################################");
            feed.entity.Where(e => e.vehicle?.trip?.route_id.Equals(vehicleId) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });

            return feed.entity.Where(t => t.vehicle?.trip?.route_id.Equals(vehicleId) ?? false).Select(t => t.vehicle).ToList();
        }

        public List<Alert> getAlerts(string routeId)
        {
            WebRequest req = WebRequest.Create(alertsUrl);
            FeedMessage feed = Serializer.Deserialize<FeedMessage>(req.GetResponse().GetResponseStream());

            Debug.WriteLine("\n################################   ALERTS   ##################################################");
            feed.entity.Where(e => e.alert?.informed_entity.Any(e3 => e3.route_id.Equals(routeId)) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });


            return feed.entity.Where(e => e.alert?.informed_entity.Any(e3 => e3.route_id.Equals(routeId)) ?? false).Select(e => e.alert).ToList();
        }


        /**
            Another way of getting real-time data from a single feed. May be slower performance.
        */
        public List<FeedEntity> getTrapezerealtimefeed(string routeId)
        {
            WebRequest req = WebRequest.Create(trapezerealtimefeedUrl);
            FeedMessage feed = Serializer.Deserialize<FeedMessage>(req.GetResponse().GetResponseStream());

            Debug.WriteLine("\n################################   TRIPS   ##################################################");
            feed.entity.Where(e => e.trip_update?.trip?.route_id.Equals(routeId) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });

            Debug.WriteLine("\n################################   VEHICLE POSITIONS   ##################################################");
            feed.entity.Where(e => e.vehicle?.trip?.route_id.Equals(routeId) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });

            Debug.WriteLine("\n################################   ALERTS   ##################################################");
            feed.entity.Where(e => e.alert?.informed_entity.Any(e3 => e3.route_id.Equals(routeId)) ?? false).ToList().ForEach(e2 =>
            {
                Debug.WriteLine(JsonConvert.SerializeObject(e2));
            });

            return feed.entity;
        }

    }
}
