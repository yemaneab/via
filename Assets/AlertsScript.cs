﻿using System.Collections.Generic;
using System.Text;
using Assets.Vuforia.Scripts;
using Assets.Vuforia.Scripts.via.gtfs;
using TMPro;
using UnityEngine;

public class AlertsScript : MonoBehaviour
{
    GameObject alertsTextObj;
    GameObject alertsDetailTextObj;
    StringBuilder alertsText = new StringBuilder();
    StringBuilder alertsDetailsText = new StringBuilder();

    // Start is called before the first frame update
    void Start()
    {


        ///////
        //GtfsService gtfs = new GtfsService();

        //var alerts2 = gtfs.getAlerts("90");
        //if (alerts2 != null && alerts2.Count > 0)
        //{
        //    alerts2.ForEach(a =>
        //    {
        //        alertsText.AppendLine("*" + a.header_text.translation[0].text);
        //        alertsDetailsText.AppendLine("* " + a.header_text.translation[0].text)
        //            .AppendLine("** " + a.description_text.translation[0].text)
        //           .AppendLine("--------");
        //    });

        //}

        /// //////
        alertsTextObj = GameObject.Find("AlertsText");
        alertsDetailTextObj = GameObject.Find("AlertsDetailText");

        var service = new OAuthTokenServiceV2();

        var alerts = service.getAlertsForRoute("90");


        if (alerts != null && alerts.Count > 0)
        {
            alerts.ForEach(a =>
            {
              alertsText.AppendLine("*" + a.name);
               alertsDetailsText.AppendLine("* " + a.name).AppendLine("** " + a.description).AppendLine("--------");
            });

        }

        alertsTextObj.GetComponent<TextMeshPro>().text = alertsText.ToString();
        alertsDetailTextObj.GetComponent<TextMeshPro>().text = alertsDetailsText.ToString();

         alertsDetailTextObj.active = false;


    }

    // Update is called once per frame
    void Update()
    {    

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;

            if (Physics.Raycast(ray, out Hit))
            {
                string btnName = Hit.transform.name;
                switch (btnName)
                {
                    case "caution_cone":
                        if (alertsDetailTextObj.active)
                        {
                            alertsDetailTextObj.SetActive(false);
                            alertsTextObj.SetActive(true);
                        }
                        else
                        {
                            alertsDetailTextObj.SetActive(true);
                            alertsTextObj.SetActive(false);
                        }
                        break;

                }

            }
        }
    }
}
