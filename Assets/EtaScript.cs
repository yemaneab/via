﻿using System;
using Assets.Vuforia.Scripts;
using TMPro;
using UnityEngine;

public class EtaScript : MonoBehaviour
{
    GameObject etaTextObj;
     
    // Start is called before the first frame update
    void Start()
    {

         etaTextObj = GameObject.Find("Time Left");

        var service = new OAuthTokenServiceV2();

        var estimatedArrival = service.getBusLocations("92229", "90", "1")[0].eta;

        var etaText = DateTime.Parse(estimatedArrival).ToUniversalTime().ToLongTimeString();


        etaTextObj.GetComponent<TextMeshPro>().text = etaText;
           
    }

    // Update is called once per frame
    void Update()
    {
        
    }

  

}
